#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <iomanip>
#include <math.h>

bool CHANGE_NUMBER = false;  // false - мінімальне значення, true - середне значення;

using namespace std;

void changeArr(int* arr, int position1, int position2) {
    int sum = 0;
    int count = 0;

    int minNumber;
    for (int i = position1; i <= position2; i++) {
        sum += arr[i];
        count++;
        if (i == position1 || arr[i] < minNumber) {
            minNumber = arr[i];
        }
    }
    float middleNum = (float) sum / (float) count;

    for (int i = position1; i <= position2; i++) {
        cout << arr[i] << ", ";
        if (arr[i] < round(middleNum)) {
            arr[i] = CHANGE_NUMBER ? round(middleNum) : minNumber;
        }

    }
    cout << setw(20) << "Середнє значення: " << middleNum << endl;
}

int main() {
    srand(time(0));
    const int n = 200;
    int arr[n];

    int realN;
    cout << "Введіть розмірність масиву: ";
    cin >> realN;

    cout << "Вхідний масив: [";
    for (int i = 0; i < realN; i++) {
        arr[i] = -100 + rand() % (100 + 100 + 1);
        cout << arr[i] << ", ";
    }
    cout << "\b\b]" << endl;

    bool setPos1 = false;
    bool setPos2 = false;
    int pos1, pos2;
    for (int i = 0; i < realN; i++) {
        if ((setPos2 == false && setPos1 == false) && arr[i] >= 0) {
            pos1 = i;
            setPos1 = true;
        }
        if (setPos1 == true && arr[i] < 0) {
            pos2 = i - 1;
            setPos2 = true;
        }
        else if (i == realN - 1 && arr[realN - 1] >= 0) {
            pos2 = i;
            setPos2 = true;
        }
        if (setPos1 == true && setPos2 == true) {
            changeArr(arr, pos1, pos2);
            setPos1 = false;
            setPos2 = false;
        }
    }

    cout << "Вихідний масив: [";
    for (int i = 0; i < realN; i++) {
        cout << arr[i] << ", ";
    }
    cout << "\b\b]" << endl;

}
